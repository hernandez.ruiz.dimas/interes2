/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interes;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author Dimas
 */
public class Interes {

    private static double getIntereses(double capital, double porcentaje) {
        return capital * porcentaje / 100;
    }
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        entrada.useLocale(Locale.UK);
        System.out.print("introduce el capital: ");
        double capital = entrada.nextDouble();
        System.out.print("introduce el porcentaje de interes anual: ");
        double porcentaje = entrada.nextDouble();
        System.out.print("introduce el número de años: ");
        int anyos = entrada.nextInt();
        double interesesTotales = 0;
        int elAnyo = 1;
        while (elAnyo <= anyos) {
        elAnyo++;
        interesesTotales += getIntereses(capital, porcentaje);
        capital = capital + getIntereses(capital, porcentaje);
        System.out.printf("año: %d > interes = %.2f capital = %.2f\n",
        elAnyo---1, getIntereses(capital, porcentaje), capital);
        }
        System.out.println("has ganado " + interesesTotales + "€");
    }
}